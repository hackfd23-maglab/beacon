# Beacon

Der Beacon wurde unter Raspbian auf einem Raspberry Pi 3B+ mit Ethernetanschluss getestet.

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

## WLAN Monitor Mode

Um Geräte in der Nähe entdecken zu können, muss ein externes WLAN-Interface verwendet werden, das den Monitor-Mode unterstützt. Für die Verwendung mit dem TL-WN722N muss hierfür ein gepatchter Treiber installiert werden: https://github.com/aircrack-ng/rtl8188eus

1. Mittels `iwconfig` den Namen des externen WLAN-Interface ermitteln. Dieser ist meist "wlan1".
2. `sudo ifconfig wlan1 down`
3. `sudo iwconfig wlan1 mode monitor`
4. `sudo ifconfig wlan1 up`
5. Mittels `iwconfig` kann überprüft werden, ob der mode zu Mode:Monitor geändert wurde.

## Autostart 

Falls der name des externen WLAN-Interfaces nicht "wlan1" ist muss dieser entsprechend in `start_donation` geändert werden.

`sudo cp start_donation /etc/init.d/`

`sudo chmod 755 /etc/init.d/start_donation/`

`cp readStream.pl /home/pi/readStream.pl`

## Parsing script

- Benötige debian packages: `sudo apt install tshark perl liblist-moreutils-perl libdigest-sha-perl libwww-perl libhttp-message-perl libjson-perl`
- Erstelle eine `.env` Datei in `/home/pi/` und füge den Authorization-Token des Beacon hinzu.

## License
This project is MIT licensed.
