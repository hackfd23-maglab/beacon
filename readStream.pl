#!/bin/perl

use strict;
use warnings;
use utf8;

use Data::Dumper;
use Digest::SHA qw(sha256_hex);
use List::MoreUtils qw(uniq);
use LWP::UserAgent;
use HTTP::Request;
use JSON;

my $LWP = LWP::UserAgent->new(); 
my $URL = 'http://smctl.de:8080/beacon/data';

# Read beacon token from local .env file
my $Authorization;
open(my $IO, '<', '.env') or die "Could not open .env file!";
{
    $Authorization = <$IO>;
}
close($IO);

die 'Please create .env file and add the Authorization token!' if !defined($Authorization);

# Create a hash for all MAC addresses
my $Hash;
my @Array;

while( my $Line = <STDIN> ) {
    # example lines:
    # 2366 24.967086230 b2:a2:21:b5:0a:64 → ff:ff:ff:ff:ff:ff 802.11 212 Probe Request, SN=1171, FN=0, Flags=........C, SSID=Wildcard (Broadcast)
    # 2368 24.971683672 b2:a2:21:b5:0a:64 → ff:ff:ff:ff:ff:ff 802.11 221 Probe Request, SN=1172, FN=0, Flags=........C, SSID="GuestWLAN"

    # Skip broadcast
    next if $Line =~ m/SSID=Wildcard.\(Broadcast\)/xms;

    # Parse MAC address
    $Line =~ m/(([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2}))/xms;
    my $MAC = $1;

    # Parse SSID
    $Line =~ m/SSID="(.{0,})"/xms;
    my $SSID = $1;

    #print "mac: $MAC - ssid: $SSID\n";

#     $Hash->{$MAC}{$SSID} = sha256_hex($SSID);
    $Hash->{$MAC}{$SSID} = $SSID;
}

for my $Key (keys %{$Hash}) {
    my $Hashed = '["';
    for my $SSID (sort keys %{ $Hash->{$Key}} ) {
        $Hashed .= sha256_hex($SSID).',';
        # $Hashed .= $SSID.',';

    }
    $Hashed .= '"]';

    # Replace last komma
    $Hashed =~ s/,"]$/"]/;

    push @Array, $Hashed;
}

# Remove dublicates
@Array = uniq(@Array);

my $JSON = '[';

for my $Key (@Array) {
    $JSON .= $Key.', ';
}

    # Replace last komma
    $JSON =~ s/, $//;

$JSON .= ']';

print STDERR Dumper $JSON;


my $Request = new HTTP::Request( 
    'POST' => $URL, 
    [
        'Content-Type'  => 'application/json; charset=UTF-8',
        'Authorization' => $Authorization, 
    ],
    $JSON,
);
$LWP->request($Request);

my $Return = $LWP->request($Request)->as_string(); 

print STDERR Dumper $Return;

close $IO;
